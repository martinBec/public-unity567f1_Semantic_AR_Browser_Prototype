﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mockBrowserBehaviour : MonoBehaviour {

	public Button searchButton;
	public GameObject searchResultsView;
	public GameObject procedureStepsView;
	public StepCardViewBehaviour stepCardView;
	public Button spicyGrilledChickenResult;
	public Button onNextButton;
	public Button onPreviousButton;
	public StepViewBehaviour firstResult;
	public GameObject[] resultsToHide;

	// Use this for initialization
	void Start () {
		this.searchButton.onClick.AddListener(onSearchButtonTapped);
		this.spicyGrilledChickenResult.onClick.AddListener(onResultButtonTapped);
		this.onPreviousButton.onClick.AddListener(onPreviousButtonTapped);
		this.onNextButton.onClick.AddListener(onNextButtonTapped);
		this.stepCardView.gameObject.SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void onSearchButtonTapped() {
		bool isActive = this.searchResultsView.activeSelf;
		this.searchResultsView.SetActive(!isActive);
	}

	void onResultButtonTapped() {
		this.searchResultsView.SetActive(false);
		this.procedureStepsView.SetActive(true);
		this.stepCardView.gameObject.SetActive(true);
		this.onPreviousButtonTapped();
	}

	void onPreviousButtonTapped() {
		
//		this.stepCardView.configure ("Preparation of  chicken",
//			"Slightly dry the chicken breats and remove any excess fat.",
//			"Ashlynn Herwitz"
//		);

		//this.firstResult.checkStep(false);
		this.showResults(false);
	}

	void onNextButtonTapped() {
		
//		this.stepCardView.configure ("Preparation of  sauce",
//			"Whisk the juice of 1 lemon, Worcestershire sauce and 2 tablespoons of olive oil in a bowl.",
//			"Ashlynn Herwitz"
//		);

		//this.firstResult.checkStep(true);
		this.showResults(true);
	}

	void showResults(bool show) {
	
		foreach (GameObject go in resultsToHide) {
			go.SetActive(show);
		}
	}
}
