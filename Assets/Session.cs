﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Session : MonoBehaviour {

	private string username = "martinTest";
	private string password = "123456";
	private string token;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public string getUsername() {
		return this.username;
	}

	public string getPassword() {
		return this.password;
	}
		
	public string getoken() {
		return this.token;
	}

	public void parseLoginResponse(string json) {
		JSONMapper<LoginResponse> mapper = new JSONMapper<LoginResponse>();
		LoginResponse response = mapper.parse(json);
		this.token = response.token;
	}
}
