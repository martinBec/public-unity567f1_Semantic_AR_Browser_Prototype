﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ProcedureStepListView : SemanticARBrowserSDKWidget {

	public GameObject content;
	public Text procedureTitle;
	public Button previousButton;
	public Button nextButton;
	public StepMappingTask stepMappingTask;
	public GameObject view;
	private Stack<GameObject> steps = new Stack<GameObject>();

	public override string identifier() {
		return "procedure_step_list_view";
	}

	// Use this for initialization
	void Start () {
		this.view.SetActive(false);
		this.previousButton.onClick.AddListener(this.previousAction);
		this.nextButton.onClick.AddListener(this.nextAction);
	}
		
	// Update is called once per frame
	void Update () {
		
	}

	public override void perform(EventMessage eventMessage) {
		switch(eventMessage.topic) {
		case "load_procedure_on_list":
			this.loadProcedure(eventMessage);
			break;
		case "load_step_on_list":
			this.addStep(eventMessage);
			break;
		case "remove_step_from_list":
			this.removeLastStep();
			break;

		}
	}

	void loadProcedure(EventMessage eventMessage) {
		LoadProcedureOnListEventMessage loadProcedureOnListEventMessage = (LoadProcedureOnListEventMessage) eventMessage;
		this.showView();
		this.removeAllSteps();
		this.procedureTitle.text = loadProcedureOnListEventMessage.procedureTitle;
	}	
		
	void showView() {
		this.view.SetActive(true);
	}

	void removeAllSteps() {
		foreach (Transform child in this.content.transform) {
			GameObject.Destroy (child.gameObject);
		}
		this.steps.Clear();
	}
		
	void addStep(EventMessage eventMessage) {
		this.checkCurrentStep(true);
		LoadStepOnListEventMessage listEventMessage = (LoadStepOnListEventMessage) eventMessage;

		if (listEventMessage.isLastStep) {
			return;
		}

		GameObject stepGO = this.stepMappingTask.perform(listEventMessage.step, this.eventDispatcher);
		this.steps.Push(stepGO);
		stepGO.transform.SetParent(content.transform);
	}

	public void nextAction() {
		this.send(NextStepEventMessage.build());
	}

	public void previousAction() {
		this.send(PreviousStepEventMessage.build());
	}

	void removeLastStep() {
		GameObject child = this.steps.Pop();
		GameObject.Destroy(child.gameObject);
		this.checkCurrentStep(false);
	}
		
	void checkCurrentStep(bool check) {
		if (this.steps.Count == 0) { return; }
		GameObject stepObj = this.steps.Peek();
		StepViewBehaviour stepView = stepObj.GetComponent<StepViewBehaviour> ();
		stepView.checkStep(check);
	}
}
