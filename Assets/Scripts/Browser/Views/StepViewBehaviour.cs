﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class StepViewBehaviour : MonoBehaviour {

	public Text title;
	public Image image;
	public Button button;
	EventDispatcher eventDispatcher;

	// Use this for initialization
	void Start () {
		this.button.onClick.AddListener(stepTappedAction);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void configure(string text, string image, EventDispatcher eventDispatcher) {
		this.title.text = text;
		this.eventDispatcher = eventDispatcher;
	}

	public void checkStep(bool check) {
		string baseSpriteUrl = "Images/UI/";
		if (check) {
			Sprite selectedImage = Resources.Load<Sprite>(baseSpriteUrl + "check");
			this.image.sprite = selectedImage;
		} else {
			Sprite targetImage = Resources.Load<Sprite>(baseSpriteUrl + "target");
			this.image.sprite = targetImage;
		}
	}

	public void stepTappedAction() {
		this.checkStep(true);
		this.eventDispatcher.dispatch(NextStepEventMessage.build());
	}
}
