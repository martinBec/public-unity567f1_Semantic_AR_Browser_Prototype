﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StepCardViewBehaviour : MonoBehaviour, IEventReceiver {
	
	public Text  stepTitle;
	public Text  stepDescription;
	public Image userImage;
	public Text  username; 

	// Use this for initialization
	void Start () {
		this.gameObject.SetActive(false);
	}
		
	// Update is called once per frame
	void Update () {
		
	}

	public string identifier() {
		return "step_card_view";
	}

	public void perform(EventMessage eventMessage) {
		switch (eventMessage.topic) {
		case "load_step_description":
			this.configure(eventMessage);
			break;
		}
	}

	void configure(EventMessage eventMessage) {
		LoadStepDescriptionEventMessage loadStepDescriptionEventMessage = (LoadStepDescriptionEventMessage)eventMessage;
		this.stepTitle.text = loadStepDescriptionEventMessage.title;
		this.stepDescription.text = loadStepDescriptionEventMessage.description;
		this.username.text = loadStepDescriptionEventMessage.username;
		this.gameObject.SetActive(true);
	}
}
