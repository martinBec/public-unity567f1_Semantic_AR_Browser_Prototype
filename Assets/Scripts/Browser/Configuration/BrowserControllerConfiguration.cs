﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class BrowserControllerConfiguration : SemanticARBrowserSDKConfiguration
{
	public BrowserController browserController;
	public ProcedureStepListView procedureListView;
	public StepCardViewBehaviour stepCard;

	public override List<SemanticARBrowserSDKWidget> widgetDefinitions() {

		SemanticARBrowserSDKWidget [] widgets = { browserController, procedureListView };
		return widgets.ToList();
	}

	public override Dictionary<string, List<IEventReceiver>> eventReceiversDefinitions() {


		var receivers = new Dictionary<string, List<IEventReceiver>>()
		{
			{ BrowserEventMessageTopics.selectProcedure, new List<IEventReceiver> { browserController } },
			{ BrowserEventMessageTopics.previousStep, new List<IEventReceiver> { browserController } },
			{ BrowserEventMessageTopics.nextStep, new List<IEventReceiver> { browserController } },
			{ BrowserEventMessageTopics.loadStepDescription, new List<IEventReceiver> { stepCard } },
			{ BrowserEventMessageTopics.loadStepOnList, new List<IEventReceiver> { procedureListView } },
			{ BrowserEventMessageTopics.removeStepfromList, new List<IEventReceiver> { procedureListView } },
			{ BrowserEventMessageTopics.loadProcedureOnList, new List<IEventReceiver> { procedureListView } },
		};
			
		return receivers;
	}
}

