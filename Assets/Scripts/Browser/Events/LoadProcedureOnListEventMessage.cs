﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadProcedureOnListEventMessage : EventMessage
{
	static string kProcedureId = "procedure_title";

	public LoadProcedureOnListEventMessage(string procedureTitle) {
		this.topic = BrowserEventMessageTopics.loadProcedureOnList;
		this.receiver = EventMessage.ALL_RECEIVERS;
		this.data = new Dictionary<string,System.Object>();
		this.data.Add(kProcedureId, procedureTitle);
	}

	public string procedureTitle {
		get { 
			return (string)this.data[kProcedureId];
		}
	}
}

