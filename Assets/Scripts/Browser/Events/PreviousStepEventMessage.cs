﻿using UnityEngine;
using System.Collections;

public class PreviousStepEventMessage : EventMessage
{
	public PreviousStepEventMessage() {
		this.topic = BrowserEventMessageTopics.previousStep; 
		this.receiver = EventMessage.ALL_RECEIVERS;
	}

	public static PreviousStepEventMessage build() {
		return new PreviousStepEventMessage();
	}
}