﻿using UnityEngine;
using System.Collections;

public class NextStepEventMessage : EventMessage
{
	public NextStepEventMessage() {
		this.topic = BrowserEventMessageTopics.nextStep; 
		this.receiver = EventMessage.ALL_RECEIVERS;
	}

	public static NextStepEventMessage build() {
		return new NextStepEventMessage();
	}
}

