﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadStepDescriptionEventMessage : EventMessage
{

	private static string kTitle = "title";
	private static string kDescription = "description"; 
	private static string kUserName = "username"; 
	private static string kUserProfileUrl = "profile_url";

	public LoadStepDescriptionEventMessage(string title, string description, string userName, string userProfileUrl) {

		this.topic = BrowserEventMessageTopics.loadStepDescription; 
		this.receiver = EventMessage.ALL_RECEIVERS;
		this.data = new Dictionary<string, System.Object>();
		this.data.Add(kTitle, title);
		this.data.Add(kDescription, description);
		this.data.Add(kUserName, userName);
		this.data.Add(kUserProfileUrl, userProfileUrl);
	}
		
	public string title {
		get  { 
			return (string) data[kTitle];
		}
	} 


	public string description {
		get  { 
			return (string) data[kDescription];
		}
	}

	public string username {
		get  { 
			return (string) data[kUserName];
		}
	} 

	public string profileUrl {
		get  { 
			return (string) data[kUserProfileUrl];
		}
	} 
}

