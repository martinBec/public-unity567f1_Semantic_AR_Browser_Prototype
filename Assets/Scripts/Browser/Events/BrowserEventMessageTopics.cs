﻿using UnityEngine;
using System.Collections;

public class BrowserEventMessageTopics
{
	// Controller
	public static string selectProcedure = "select_procedure";
	public static string previousStep = "previous_step";
	public static string nextStep = "next_step";

	// Procedure List
	public static string removeStepfromList = "remove_step_from_list";
	public static string loadStepOnList = "load_step_on_list";
	public static string loadProcedureOnList = "load_procedure_on_list";

	// Procedure Step Card
	public static string loadStepDescription = "load_step_description";
}

