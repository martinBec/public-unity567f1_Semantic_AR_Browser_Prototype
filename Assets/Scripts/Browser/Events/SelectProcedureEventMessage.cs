﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectProcedureEventMessage : EventMessage
{
	static string kProcedureId = "procedure_id";

	public SelectProcedureEventMessage(string procedureId) {

		this.topic = BrowserEventMessageTopics.selectProcedure; 
		this.receiver = EventMessage.ALL_RECEIVERS;
		this.data = new Dictionary<string, System.Object>();
		this.data.Add(kProcedureId, procedureId);
	}

	public string procedureId {
		get { 
			return (string)this.data[kProcedureId];
		}
	}
}

