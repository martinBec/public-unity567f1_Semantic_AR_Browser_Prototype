﻿using UnityEngine;
using System.Collections;

public class RemoveLoadStepfromListEventMessage : EventMessage
{

	public RemoveLoadStepfromListEventMessage() {
		this.topic = BrowserEventMessageTopics.removeStepfromList;
		this.receiver = EventMessage.ALL_RECEIVERS;
	}
}

