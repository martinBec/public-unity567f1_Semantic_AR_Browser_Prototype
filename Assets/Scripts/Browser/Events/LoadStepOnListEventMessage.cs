﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadStepOnListEventMessage : EventMessage
{

	private static string kStep = "step";
	private static string kIsLastStep = "is_last_step";

	public LoadStepOnListEventMessage(Step step) {
		this.topic = BrowserEventMessageTopics.loadStepOnList; 
		this.receiver = EventMessage.ALL_RECEIVERS;
		this.data = new Dictionary<string, System.Object>();
		this.data.Add(kStep, step);
		this.data.Add(kIsLastStep, false);
	}

	public LoadStepOnListEventMessage(bool isLastStep) {
		this.topic = BrowserEventMessageTopics.loadStepOnList; 
		this.receiver = EventMessage.ALL_RECEIVERS;
		this.data = new Dictionary<string, System.Object>();
		this.data.Add(kIsLastStep, isLastStep);
	}

	public bool isLastStep {
	
		get { 
			return (bool) this.data[kIsLastStep];
		}
	}

	public Step step {
		get { 
			return (Step) this.data[kStep];
		}
	}
}

