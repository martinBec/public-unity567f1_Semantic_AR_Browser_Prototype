﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SimpleJSON;

public class FetchProcedureStepsTask : MonoBehaviour {
	
	public NetworkLayer networkLayer;
	Action<Dictionary<String,Step>> onResultMapping;

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {}

	public void perform(string localId, Action<Dictionary<String,Step>> onResultMapping) {
		this.onResultMapping = onResultMapping;
		this.networkLayer.getSteps(localId, this.mapSteps);
	}
		
	void mapSteps(SemanticGraph semanticGraph) {

		Dictionary<String,Step> stepDictionary = new Dictionary<String,Step>();

		foreach (JSONObject entity in semanticGraph.statements) {
			Step step = new Step(entity);
			stepDictionary.Add(step.getId(), step);
		}

		this.onResultMapping(stepDictionary);
		this.onResultMapping = null;
	}


}
