﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class FetchProcedureDetailTask : MonoBehaviour
{
	public NetworkLayer networkLayer;
	Action<Procedure> onResultMapping;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void perform(string localId, Action<Procedure> onResultMapping) {
		this.onResultMapping = onResultMapping;
		this.networkLayer.getProcedureDetail(localId, this.onProcedureDetailMapping);
	}

	public void onProcedureDetailMapping(SemanticGraph procedureDetail) {
		Procedure procedure = Procedure.detail(procedureDetail);
		onResultMapping(procedure);
		onResultMapping = null;
	}
}

