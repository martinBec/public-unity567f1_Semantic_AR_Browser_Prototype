﻿using UnityEngine;
using System.Collections;
using System;

public class StepMappingTask : MonoBehaviour
{
	public GameObject stepPrefab;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public GameObject perform(Step step, EventDispatcher eventDispatcher) {
		GameObject stepObj = (GameObject) Instantiate(stepPrefab, new Vector3 (0, 0, 0), Quaternion.identity);
		StepViewBehaviour stepView = stepObj.GetComponent<StepViewBehaviour> ();
		stepView.configure(step.getTitle(), step.getImageUrl(), eventDispatcher);
		return stepObj;
	}
}

