﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using SimpleJSON;

public class SearchProcedureTask : MonoBehaviour {

	public NetworkLayer networkLayer;
	public GameObject searchResultPrefab;
	EventDispatcher eventDispatcher;

	// Use this for initialization
	void Start () {}

	// Update is called once per frame
	void Update () {}


	public void perform(string keyword, EventDispatcher eventDispatcher) {
		this.eventDispatcher = eventDispatcher;
		this.networkLayer.searchProcedures(keyword, mapProceduresToViews);
	}

	public void mapProceduresToViews(SemanticGraph proceduresFound) {

		List<GameObject> viewList = new List<GameObject> ();

		foreach (JSONObject entity in proceduresFound.statements) {
			SearchProcedureResult result = new SearchProcedureResult (Procedure.listItem(entity));
			GameObject procedureObj = (GameObject) Instantiate (searchResultPrefab, new Vector3 (0, 0, 0), Quaternion.identity);
			SearchResultItemViewBehaviour searchItemView = procedureObj.GetComponent<SearchResultItemViewBehaviour> ();
			searchItemView.configure(result, this.eventDispatcher);
			viewList.Add(procedureObj);
		}

		this.performShowProcedureEvent(viewList);
	}

	public void performShowProcedureEvent(List<GameObject> viewList) {
		ShowProcedureResultsListEventMessage showProcedureMessage = new ShowProcedureResultsListEventMessage(viewList);
		this.eventDispatcher.dispatch(showProcedureMessage);
	}
}
