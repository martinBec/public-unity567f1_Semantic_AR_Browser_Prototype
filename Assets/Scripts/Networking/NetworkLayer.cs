﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.IO;
using SimpleJSON;
using System;



public class NetworkLayer : MonoBehaviour {

	public SemanticProceduresARBrowserNetworkingContext networkingContext;
	Action<SemanticGraph> onSemanticAction;
	Action<SemanticGraph> onStepsSemanticAction;


	// Use this for initialization
	void Start() {
		this.networkingContext.login();
	}
	
	// Update is called once per frame
	void Update() {
		
	}
		
	public void searchProcedures(string keyword, Action<SemanticGraph> onSuccess) {

		this.onSemanticAction = onSuccess;
		String fullPath = this.networkingContext.searchProceduresEndpoint(keyword);
		Dictionary<string,string> headers = this.networkingContext.authorizationHeaderDefinition();
		Action<String> onSuccessAction = this.onSemanticSuccessResponse;
		Request request = new Request (fullPath, RequestTypeEnum.GET, headers, "");
		StartCoroutine(request.perform (onSuccessAction));
	}

	public void getProcedureDetail(string procedureLocalId, Action<SemanticGraph> onSuccess) {

		this.onSemanticAction = onSuccess;
		Dictionary<string,string> headers = this.networkingContext.authorizationHeaderDefinition();
		String fullPath = this.networkingContext.procedureDetailEndpoint(procedureLocalId);
		Action<String> onSuccessAction = this.onSemanticSuccessResponse;
		Request request = new Request (fullPath, RequestTypeEnum.GET, headers, "");
		StartCoroutine(request.perform (onSuccessAction));
	}

	public void getSteps(string procedureLocalId, Action<SemanticGraph> onSuccess) {

		this.onStepsSemanticAction = onSuccess;
		Dictionary<string,string> headers = this.networkingContext.authorizationHeaderDefinition();

		String fullPath = this.networkingContext.procedureProcedureStepsEndpoint(procedureLocalId);
		Action<String> onSuccessAction = this.onStepsSemanticSuccessResponse;
		Request request = new Request (fullPath, RequestTypeEnum.GET, headers, "");
		StartCoroutine(request.perform(onSuccessAction));
	}
		
	public void onSemanticSuccessResponse(string json) {
		this.onSemanticAction(SemanticGraph.parse(json));
		this.onSemanticAction = null;
	}

	public void onStepsSemanticSuccessResponse(string json) {
		this.onStepsSemanticAction(SemanticGraph.parse(json));
		this.onStepsSemanticAction = null;
	}
}
