﻿using UnityEngine;
using System.Collections;

public enum RequestTypeEnum : int
{
	GET = 0,
	POST = 1,
	PUT = 2,
	DELETE = 3
}

