﻿using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using System.Text;
using System.Collections;
using System;
using System.IO;


public partial class Request
{
	string endpoint;
	RequestTypeEnum type;
	Dictionary<string, string> headers;
	string jsonBody;

	public Request (string endpoint,RequestTypeEnum type, Dictionary<string, string> headers, string jsonBody) {
		this.endpoint = endpoint;
		this.type = type;
		this.headers = headers;
		this.jsonBody = jsonBody;
	}
		
	private UnityWebRequest build() {
		return new RequestBuilder(this).build();
	}
		
	public IEnumerator perform(Action<string> onSuccess) {

		Debug.Log ("performing request to: " + this.endpoint +  "\nheaders: " + this.headers + "\nbody: " + this.jsonBody);

		using (UnityWebRequest req = build())
			{
				yield return req.Send();
				while (!req.isDone)
				yield return new WaitForSeconds(1.0f);
				byte[] result = req.downloadHandler.data;
				string responseJson = System.Text.Encoding.Default.GetString(result);
				Debug.Log ("Response: " + responseJson);
				onSuccess(responseJson);
				yield return 0;
			}
	}
		
 	class RequestBuilder {

		Request request;

		public RequestBuilder(Request request) {
			this.request = request;
		}

		private UnityWebRequest addHeaders(UnityWebRequest request) {

			request.SetRequestHeader("Accept","application/json");

			foreach (KeyValuePair<string, string> kvp in this.request.headers) {
				request.SetRequestHeader(kvp.Key, kvp.Value);
			}

			return request;
		}

		UnityWebRequest buildGet() {
			return addHeaders(UnityWebRequest.Get(this.request.endpoint));
		}

		UnityWebRequest buildPost() {
			UnityWebRequest request = UnityWebRequest.Post (this.request.endpoint, "POST");
			return addHeaders(buildBody(request));
		}

		UnityWebRequest buildPut() {
			UnityWebRequest request = UnityWebRequest.Put(this.request.endpoint, "PUT");
			return addHeaders(buildBody(request));
		}

		UnityWebRequest buildDelete() {
			return addHeaders(UnityWebRequest.Delete(this.request.endpoint));
		}
			
		UnityWebRequest buildBody(UnityWebRequest request) {

			if (this.request.jsonBody.Length == 0) {
				return request;
			}

			byte[] bodyRaw = Encoding.UTF8.GetBytes(this.request.jsonBody);
			request.uploadHandler = (UploadHandler)new UploadHandlerRaw (bodyRaw);
			request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
			request.SetRequestHeader("Content-Type","application/json");

			return request;
		}

		public UnityWebRequest build() {
			
			switch (this.request.type) {
			case RequestTypeEnum.POST:
				return buildPost();
				break;
			case RequestTypeEnum.DELETE:
				return this.buildDelete();
				break;
			case RequestTypeEnum.PUT:
				return this.buildPut();
				break;
			default:
				return this.buildGet(); 
				break;
			}
		}
	}
}


