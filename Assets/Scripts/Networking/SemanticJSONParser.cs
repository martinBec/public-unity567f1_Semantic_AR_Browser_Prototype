﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

public class SemanticJSONParser
{

	public static List<Subject> parse(string jsonStr) {
		JSONNode json = SimpleJSON.JSON.Parse(jsonStr);
		return parseSemanticJSON(json);
	}

	private static List<Subject> parseSemanticJSON(JSONNode json) {

		List<Subject> subjects = new List<Subject>();

		foreach (KeyValuePair<string, JSONNode> subjectTuples in json) {

			List<Property> properties = new List<Property> ();

			foreach (KeyValuePair<string, JSONNode> propertyTuples in subjectTuples.Value) {
				List<Value> values = new List<Value>();

				foreach (JSONNode value in propertyTuples.Value.AsArray) {
					values.Add (new Value(value["value"],value["type"],value["datatype"]));
				}

				string propertyLocalId = extractLocalIdFrom (propertyTuples.Key);

				if (propertyLocalId  != null ) {
					Debug.Log("Property: " + propertyLocalId);
					properties.Add(new Property (propertyLocalId, values));
				}
			}

			string subjectLocalId = extractLocalIdFrom(subjectTuples.Key);

			if (subjectLocalId != null) {
				Debug.Log("Subject: " + subjectLocalId);
				subjects.Add(new Subject(subjectLocalId,properties));	
			}
		}

		return subjects;
	}

	private static string extractLocalIdFrom(string iri) {
		string[] iriParts = iri.Split('#'); 

		if (iriParts.Length == 2) {
			return iriParts [1];
		}

		return null;
	} 
}

