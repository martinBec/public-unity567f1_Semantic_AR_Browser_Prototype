﻿using UnityEngine;
using System.Collections;

public class JSONMapper<T>
{

	public JSONMapper() {
	}

	public string serialize(T obj) {
		return JsonUtility.ToJson (obj);
	}

	public T parse(string json) {
		return JsonUtility.FromJson<T> (json);
	}
}

