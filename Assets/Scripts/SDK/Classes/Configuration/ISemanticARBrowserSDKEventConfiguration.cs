﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface ISemanticARBrowserSDKEventConfiguration
{
	Dictionary<string, List<IEventReceiver>> eventReceiversDefinitions();
}

