﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SemanticARBrowserSDKConfiguration: MonoBehaviour, ISemanticARBrowserSDKEventConfiguration, ISemanticARBrowserSDKWidgetConfiguration
{


	public virtual List<SemanticARBrowserSDKWidget> widgetDefinitions() {
		return  new List<SemanticARBrowserSDKWidget>();
	}

	public virtual Dictionary<string, List<IEventReceiver>> eventReceiversDefinitions() {
		return new Dictionary<string, List<IEventReceiver>>();
	} 
		
}

