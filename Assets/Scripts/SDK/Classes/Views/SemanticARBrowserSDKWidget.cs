﻿using UnityEngine;
using System.Collections;

public class SemanticARBrowserSDKWidget : MonoBehaviour, IEventReceiver, IEventSender
{

	public EventDispatcher eventDispatcher;

	public virtual string identifier() {
		return "To implement in childs";
	}

	public void init(EventDispatcher eventDispatcher) {
		this.eventDispatcher = eventDispatcher;
	}

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public virtual void perform(EventMessage eventMessage) {
		// To implement in childs
	}

	public void send(EventMessage eventMessage) {
		eventMessage.sender = this.identifier();
		this.eventDispatcher.dispatch(eventMessage);
	}
}

