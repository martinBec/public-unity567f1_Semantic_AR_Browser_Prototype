﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SemanticARBrowserSDKWidgetConfigurator : MonoBehaviour
{
	public EventProcessor eventProcessor;

	public void configureWidgets(ISemanticARBrowserSDKWidgetConfiguration configuration) {

		List<SemanticARBrowserSDKWidget> widgets = configuration.widgetDefinitions();

		foreach (SemanticARBrowserSDKWidget widget in widgets) {
			widget.init(new EventDispatcher (eventProcessor));
		} 
	}
}

