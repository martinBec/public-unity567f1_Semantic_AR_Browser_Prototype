﻿using UnityEngine;
using System.Collections;

public interface IEventSender
{
	string identifier();
	void send(EventMessage eventMessage);
}

