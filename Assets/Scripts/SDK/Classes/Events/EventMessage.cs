﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventMessage
{
	public static string ALL_RECEIVERS = "[ALL]";
	public string topic;
	public string receiver;
	public string sender;
	public Dictionary<string, System.Object> data;
}

