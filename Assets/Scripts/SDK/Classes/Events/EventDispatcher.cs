﻿using UnityEngine;
using System.Collections;

public class EventDispatcher
{
	EventProcessor eventProcessor;

	public EventDispatcher(EventProcessor eventProcessor){
		this.eventProcessor = eventProcessor;
	}

	public void dispatch(EventMessage eventMessage) {
		this.eventProcessor.process(eventMessage);
		Debug.Log("Event dispatched to event topic:" + eventMessage.topic);
	}
}

