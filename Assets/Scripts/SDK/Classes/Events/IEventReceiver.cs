﻿using UnityEngine;
using System.Collections;

public interface IEventReceiver 
{
	string identifier();
	void perform(EventMessage eventMessage); 
}

