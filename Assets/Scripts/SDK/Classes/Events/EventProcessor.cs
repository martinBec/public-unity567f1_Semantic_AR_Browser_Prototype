﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventProcessor: MonoBehaviour
{
	Dictionary<string, EventTopic> topics = new Dictionary<string, EventTopic>();

	public void register(ISemanticARBrowserSDKEventConfiguration configuration) {

		Dictionary<string, List<IEventReceiver>> eventReceivers = configuration.eventReceiversDefinitions();

		foreach (string topicName in eventReceivers.Keys) {
			this.registerReceivers(topicName, eventReceivers[topicName]);
		}

	}

	private void registerReceivers(string topicName, List<IEventReceiver> receivers) {
		
		EventTopic topic;

		if (topics.ContainsKey (topicName)) {
			topic = topics[topicName];
		} else {
			topic = new EventTopic(topicName);
			this.topics.Add(topicName, topic);
		}

		foreach (IEventReceiver receiver in receivers) {
			topic.addEventReceiver(receiver);
		}
	}
		
	public void process(EventMessage eventMessage) {
		StartCoroutine(this.sendMessageToTopic(eventMessage));
	}

	private IEnumerator sendMessageToTopic(EventMessage eventMessage) {

		if (eventMessage.topic == null) {
			Debug.Log("Error: Event message doesn't have any topic assigned.");
			yield return 0;
		}

		if (this.topics.ContainsKey (eventMessage.topic)) {
			Debug.Log ("Topic " + eventMessage.topic + " found. Event will be sent.");
			EventTopic topic = this.topics [eventMessage.topic];
			topic.handle (eventMessage);
		} else {
			Debug.Log ("Topic " + eventMessage.topic + " not found.");
		}

		yield return 0;
	}
}

