﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventTopic
{
	
	public string name;
	Dictionary<string, IEventReceiver> receivers;

	public EventTopic(string topicName) {
		this.name = topicName;
		receivers = new Dictionary<string, IEventReceiver> ();
	}

	public void addEventReceiver(IEventReceiver receiver) {
		string handlerName = receiver.identifier();
		Debug.Log("Receiver " + handlerName + " added.");
		receivers.Add(handlerName, receiver);
	}

	public void handle(EventMessage eventMessage) {

		if (eventMessage.receiver == null) {
			Debug.Log("Error: Event message " + eventMessage.topic + " doesn't have any receiver assigned.");
			return;
		}

		if (eventMessage.receiver.CompareTo (EventMessage.ALL_RECEIVERS) == 0) {
			this.sendEventToAll (eventMessage);
		} else {
			this.sendEvent(eventMessage);
		}
	}

	private void sendEventToAll(EventMessage eventMessage) {
		foreach (IEventReceiver receiver in receivers.Values) {
			receiver.perform(eventMessage);
		}

		Debug.Log(eventMessage.topic + " sent to all receivers.");
	}

	private void sendEvent(EventMessage eventMessage) {
		IEventReceiver receiver = receivers [eventMessage.receiver];
		receiver.perform(eventMessage);
		Debug.Log(eventMessage.topic + " sent to " + receiver.identifier());
	}
}

