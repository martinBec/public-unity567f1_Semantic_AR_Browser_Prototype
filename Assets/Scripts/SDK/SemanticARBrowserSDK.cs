﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SemanticARBrowserSDK : MonoBehaviour
{
	public EventProcessor eventProcessor;
	public SemanticARBrowserSDKWidgetConfigurator widgetConfigurator;

	// Use this for initialization
	void Start() {
	}

	// Update is called once per frame
	void Update() {

	}

	public void register(SemanticARBrowserSDKConfiguration configuration) {
		this.widgetConfigurator.configureWidgets(configuration);
		this.eventProcessor.register(configuration);
	}
}

