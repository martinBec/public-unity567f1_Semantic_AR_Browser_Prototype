﻿using UnityEngine;
using System.Collections;

public class SemanticProceduresARBrowserController : MonoBehaviour
{
	public SemanticARBrowserSDK semanticARBrowserSDK;
	public BrowserControllerConfiguration browserControllerConfiguration;
	public SearchControllerConfiguration searchControllerConfiguration;


	public 
	// Use this for initialization
	void Start ()
	{
		this.semanticARBrowserSDK.register(browserControllerConfiguration);
		this.semanticARBrowserSDK.register(searchControllerConfiguration);
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

}

