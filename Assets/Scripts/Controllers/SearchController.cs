﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchController : SemanticARBrowserSDKWidget {

	public SearchProcedureTask searchTask;

	public override string identifier() {
		return "search_controller";
	}

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {}

	public override void perform(EventMessage eventMessage) {
		this.search(eventMessage);
	}

	private void search(EventMessage eventMessage) {
		
		SearchProcedureEventMessage searchProcedureEventMessage = (SearchProcedureEventMessage)eventMessage;

		string text = searchProcedureEventMessage.keyword;

		if (text.Length == 0) {
			this.send(CloseSearchBarResultEventMessage.build());
			return;
		}

		this.searchTask.perform(text, this.eventDispatcher);
	}
}
