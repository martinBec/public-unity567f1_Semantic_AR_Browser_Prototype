﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrowserController : SemanticARBrowserSDKWidget {
	
	public Model model;
	public FetchProcedureStepsTask fetchStepsTask;
	public FetchProcedureDetailTask fetchProcedureDetailTask;

	public override string identifier() {
		return "browser_controller";
	}

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {}
	
	public override void perform (EventMessage eventMessage) {

		switch (eventMessage.topic) {
		case "select_procedure":
			this.loadProcedure(eventMessage);
			break;
		case "previous_step":
			this.previousStep(eventMessage);
			break;
		case "next_step":
			this.nextStep(eventMessage);
			break;

		}
	}

	private void loadProcedure(EventMessage eventMessage) {
		this.send(CloseSearchBarResultEventMessage.build());
		SelectProcedureEventMessage selectProcedureEventMessage = (SelectProcedureEventMessage) eventMessage;
		string procedureId = selectProcedureEventMessage.procedureId;
		this.fetchProcedureDetailTask.perform(procedureId, onProcedureDetailMapping);
	}

	public void onProcedureDetailMapping(Procedure procedure) {
		this.model.loadProcedure(procedure);
		this.send(new LoadProcedureOnListEventMessage (procedure.getTitle()));
		Step step = procedure.getFirstStep();
		this.loadStep(step);
		this.fetchStepsTask.perform(procedure.getId(), this.onStepMapping);
	}

	private void onStepMapping(Dictionary<string,Step> steps) {
		this.model.loadSteps(steps);
	}

	private void previousStep(EventMessage eventMessage) {
		Step stepRemoved = this.model.popStep();

		bool areThereMoreSteps = stepRemoved != null;

		if (areThereMoreSteps) {
			this.dispatchRemoveStepOnListEvent();
		}

		Step currentStep = this.model.currentStep();
		this.dispatchLoadStepDescriptionCardEvent(currentStep);
	}
		
	private void nextStep(EventMessage eventMessage) {
		Step nextStep = this.model.nextStep();
		this.loadStep(nextStep);
	}
		
	private void loadStep(Step step) {

		bool isLastStep = step == null;

		if (isLastStep) {
			this.checkLastStep();
			return;
		}

		this.dispatchLoadStepDescriptionCardEvent(step);
		this.dispatchLoadStepOnListEvent(step);
	}
		
	private void dispatchLoadStepDescriptionCardEvent(Step step) {
		Author author = this.model.getAuthor();
		LoadStepDescriptionEventMessage loadDescriptionEventMessage = new LoadStepDescriptionEventMessage(step.getTitle (), step.getDescription (), "Ashlynn Herwitz", ""); //author.getUsername();
		this.eventDispatcher.dispatch(loadDescriptionEventMessage);
	}

	private void dispatchRemoveStepOnListEvent() {
		this.eventDispatcher.dispatch(new RemoveLoadStepfromListEventMessage());
	}

	private void dispatchLoadStepOnListEvent(Step step) {
		this.eventDispatcher.dispatch(new LoadStepOnListEventMessage(step));
	}

	private void checkLastStep() {
		this.eventDispatcher.dispatch(new LoadStepOnListEventMessage(true));
	}

}
