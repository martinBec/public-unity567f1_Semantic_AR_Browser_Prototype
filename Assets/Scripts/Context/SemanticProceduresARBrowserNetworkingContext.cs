﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SemanticProceduresARBrowserNetworkingContext : MonoBehaviour
{

	public virtual Dictionary<string,string> authorizationHeaderDefinition() {
		return new Dictionary<string,string>();
	}

	public virtual String searchProceduresEndpoint(String keyword) {
		return "";
	}
		
	public virtual String procedureDetailEndpoint(String procedureLocalId) {
		return "";
	}

	public virtual String procedureProcedureStepsEndpoint(String procedureLocalId) {
		return "";
	}
		
	public virtual void login() {
		
	}
}

