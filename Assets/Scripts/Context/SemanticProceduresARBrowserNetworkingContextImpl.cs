﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SemanticProceduresARBrowserNetworkingContextImpl : SemanticProceduresARBrowserNetworkingContext
{

	public Session session;
	String baseUrl =  "http://192.168.0.4:8080/api/v1/";

	public override Dictionary<string,string> authorizationHeaderDefinition() {
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add ("Authorization", System.String.Format ("Bearer {0}", session.getoken ()));
		return headers;
	}

	public override String searchProceduresEndpoint(String keyword) {
		String path =  System.String.Format("procedures/search?procedure_title={0}", keyword); 
		return this.baseUrl + path;
	}

	public override String procedureDetailEndpoint(String procedureLocalId) {
		String path =  System.String.Format("procedures/detail?procedure_id={0}", procedureLocalId); 
		return this.baseUrl + path;
	}

	public override String procedureProcedureStepsEndpoint(String procedureLocalId) {
		String path =  System.String.Format("procedures/steps/get?procedure_id={0}", procedureLocalId); 
		return this.baseUrl + path;
	}
		
	public override void login() {
		String path = "users/login";
		String fullPath = this.baseUrl + path;
		Dictionary<string,string> headers = new Dictionary<string,string>();
		LoginBodyObject login = new LoginBodyObject(this.session.getUsername(),this.session.getPassword());
		JSONMapper<LoginBodyObject> mapper = new JSONMapper<LoginBodyObject>();

		Request request = new Request (fullPath, RequestTypeEnum.POST, headers, mapper.serialize(login));
		Action<String> onSuccess = this.session.parseLoginResponse;
		StartCoroutine(request.perform(onSuccess));
	}

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

