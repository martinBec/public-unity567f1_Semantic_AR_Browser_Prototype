﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShowProcedureResultsListEventMessage : EventMessage
{

	private static string kResults = "results";

	public ShowProcedureResultsListEventMessage(List<GameObject> availableResults) {

		this.topic = SearchEventTopics.showProcedureResultsList; 
		this.receiver = EventMessage.ALL_RECEIVERS;
		this.data = new Dictionary<string, System.Object>();
		this.data.Add(kResults, availableResults);
	}

	public List<GameObject> availableResults {
		get { 
			return (List<GameObject>) this.data [kResults];
		}
	}

}

