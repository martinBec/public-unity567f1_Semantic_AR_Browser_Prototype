﻿using UnityEngine;
using System.Collections;

public class SearchEventTopics
{
	public static string searchProcedures = "search_procedures";
	public static string showProcedureResultsList = "show_procedures_results_list";
	public static string closeSearchBarResultList = "close_search_bar_results_list";
}

