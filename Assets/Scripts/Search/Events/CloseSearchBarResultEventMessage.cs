﻿using UnityEngine;
using System.Collections;

public class CloseSearchBarResultEventMessage : EventMessage
{

	public CloseSearchBarResultEventMessage() {
		this.topic = SearchEventTopics.closeSearchBarResultList; 
		this.receiver = EventMessage.ALL_RECEIVERS;
	}
		
	public static CloseSearchBarResultEventMessage build() {
		return new CloseSearchBarResultEventMessage();
	}
}

