﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SearchProcedureEventMessage : EventMessage
{
	private static string kKeyword = "keyword";

	public SearchProcedureEventMessage(string keyword) {
		
		this.topic = SearchEventTopics.searchProcedures; 
		this.receiver = EventMessage.ALL_RECEIVERS;
		this.data = new Dictionary<string, System.Object>();
		this.data.Add(kKeyword, keyword);
	}

	public string keyword {
		get { return (string) this.data[kKeyword]; }
	}
}

