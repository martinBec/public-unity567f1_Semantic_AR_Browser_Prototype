﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SearchControllerConfiguration : SemanticARBrowserSDKConfiguration
{
	public SearchController searchController;
	public SearchBarViewBehaviour searchBar;

	public override List<SemanticARBrowserSDKWidget> widgetDefinitions() {

		SemanticARBrowserSDKWidget [] widgets = { searchController, searchBar };
		return widgets.ToList();
	}

	public override Dictionary<string, List<IEventReceiver>> eventReceiversDefinitions() {

		var receivers = new Dictionary<string, List<IEventReceiver>>()
		{
			{ SearchEventTopics.searchProcedures, new List<IEventReceiver> { searchController } },
			{ SearchEventTopics.showProcedureResultsList, new List<IEventReceiver> { searchBar } },
			{ SearchEventTopics.closeSearchBarResultList, new List<IEventReceiver> { searchBar } },
		};

		return receivers;
	}
}

