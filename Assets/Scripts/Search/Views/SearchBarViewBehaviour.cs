﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class SearchBarViewBehaviour : SemanticARBrowserSDKWidget {

	public SearchBarTextfieldBehaviour searchBar;
	public SearchResultsViewBehaviours searchresults;
	public Button searchButton;

	public override string identifier() {
		return "search_bar_view";
	}

	// Use this for initialization
	void Start () {
		this.searchButton.onClick.AddListener(onSearchButtonTapped);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public override void perform(EventMessage eventMessage) {
		
		switch (eventMessage.topic) {

		case "close_search_bar_results_list":
			removeResults();
			break;

		case "show_procedures_results_list":
			this.addResultsFrom(eventMessage);
			break;
		}
	}

	void onSearchButtonTapped() {
		string keywords = searchBar.getText();
		SearchProcedureEventMessage eventMessage = new SearchProcedureEventMessage(keywords);
		this.send(eventMessage);
	}
		
	public void addResultsFrom(EventMessage eventMessage) {
		ShowProcedureResultsListEventMessage showProcedureEventMessage = (ShowProcedureResultsListEventMessage) eventMessage;
		List<GameObject> availableResults = showProcedureEventMessage.availableResults;
		this.searchresults.addResults(availableResults);
	}

	void removeResults() {
		this.searchresults.removeAllResults();
	}
}
