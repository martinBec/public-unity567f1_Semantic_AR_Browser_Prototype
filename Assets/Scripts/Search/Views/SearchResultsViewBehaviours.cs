﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchResultsViewBehaviours : MonoBehaviour {

	public GameObject content;

	// Use this for initialization
	void Start () {
		this.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void addResults(List<GameObject> availableResults) {
		this.removeAllResults ();
		foreach (GameObject step in availableResults) {
			step.transform.SetParent(content.transform);
		}
		this.gameObject.SetActive(true);
	}

	public void removeAllResults() {
		foreach (Transform child in this.content.transform) {
			GameObject.Destroy(child.gameObject);
		}
		this.gameObject.SetActive (false);
	}
}
