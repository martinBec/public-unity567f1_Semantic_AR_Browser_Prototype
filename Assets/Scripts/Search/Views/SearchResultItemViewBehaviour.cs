﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class SearchResultItemViewBehaviour : MonoBehaviour {

	public Text   itemTitle;
	public Button tappableZone;
	private SearchProcedureResult result;
	EventDispatcher eventDispatcher;

	void Start () {
		this.tappableZone.onClick.AddListener(onTapResult);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void configure(SearchProcedureResult result, EventDispatcher eventDispatcher) {
		this.result = result;
		this.itemTitle.text = result.getTitle();
		this.eventDispatcher = eventDispatcher;
	}

	void onTapResult() {
		string procedureId = this.result.getID();
		SelectProcedureEventMessage selectProcedureEventMessage = new SelectProcedureEventMessage(procedureId);
		this.eventDispatcher.dispatch(selectProcedureEventMessage);
	}
}
