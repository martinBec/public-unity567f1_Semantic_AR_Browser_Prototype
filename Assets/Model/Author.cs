﻿using System.Collections;
using SimpleJSON;

public class Author
{
	public const string  AUTHOR_NAME = "ars:hasUsername";

	string id;
	string username;

	public Author(JSONNode json) 
	{
		this.id = (string)json[RDFTerms.ID];
		this.username = (string)json[AUTHOR_NAME];

	} 

	//Deprecated
	public Author(Subject subject) 
	{
		this.id  = subject.getId();
		this.username = subject.getValueForProperty("hasUsername");
	}

	public string getUsername() 
	{
		return this.username;
	}
}

