﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class Step {

	public const string STEP_TITLE = "ars:hasStepTitle";
	public const string STEP_DESCRIPTION = "ars:hasStepDescription";
	public const string NEXT_STEP = "ars:nextStep";

	private string id;
	private string title;
	private string imageUrl;
	private string description;
	private string nextStep;

	public Step(JSONNode stepJson) {
		this.id = stepJson[RDFTerms.ID];
		this.title = stepJson[STEP_TITLE] ?? "";
		this.description = stepJson[STEP_DESCRIPTION] ?? "";
		this.imageUrl = "";

		if (stepJson.HasKey(NEXT_STEP)) {
			this.nextStep = stepJson[NEXT_STEP][RDFTerms.ID];
		}
	}

	//Deprecated
	public Step(Subject subject) {
		this.id = subject.getId();
		this.title = subject.getValueForProperty ("hasStepTitle");
		string imageUrl = subject.getValueForProperty ("hasImageURL");

		if (imageUrl != null) {
			this.imageUrl = imageUrl;
		}
		else {
			this.imageUrl = "";
		}

		this.description = subject.getValueForProperty("hasStepDescription");
		this.nextStep = subject.getValueForProperty("nextStep");
	}

	public Step(string title,string imageUrl,string description) {
		this.title = title;
		this.imageUrl = imageUrl;
		this.description = description;
	}

	public string getId() {
		return id;
	}

	public string getTitle () {
		return title;
	}

	public string getImageUrl() {
		return imageUrl;
	}

	public string getDescription() {
		return description;
	}

	public string getNextStep() {
		
		string nextStep = "";

		if (this.nextStep != null) {
			nextStep = this.nextStep;
		}

		return nextStep;
	}
}
