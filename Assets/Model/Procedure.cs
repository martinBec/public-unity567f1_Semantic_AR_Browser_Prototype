﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class Procedure
{

	public const string  PROCEDURE_TITLE = "ars:hasProcedureTitle";
	public const string  PROCEDURE_DESCRIPTION = "ars:hasProcedureDescription";
	public const string  PROCEDURE_AUTHOR = "ars:hasAuthor";

	private string id;
	private string title;
	private string description;
	private Author author;
	private Step firstStep;

	public Procedure() {
		
	}

	public static Procedure listItem(JSONNode procedureJson) 
	{
		Procedure procedure = new Procedure();
		procedure.id = (string) procedureJson[RDFTerms.ID];
		procedure.title = (string) procedureJson[Procedure.PROCEDURE_TITLE];
		return procedure;
	}

	public static Procedure detail(SemanticGraph graph) 
	{
		
		Procedure procedure = new Procedure();

		foreach (JSONObject entity in graph.statements) {
			
			string type = entity[RDFTerms.TYPE];

			switch (type) {

			case OntologyTerms.PROCEDURE:
				procedure.id = entity[RDFTerms.ID];
				procedure.title = entity[PROCEDURE_TITLE];
				procedure.description = entity[PROCEDURE_DESCRIPTION];
				break;

			case OntologyTerms.STEP:
				procedure.firstStep = new Step(entity);
				break;

			case OntologyTerms.AUTHOR:
				procedure.author = new Author(entity);
				break;
				
			}
		}

		return procedure;
	}

	//Deprecated
	public Procedure(List<Subject> procedureSubjects) {

		foreach (Subject subject in procedureSubjects) {

			string typeValue = subject.getValueForProperty ("type");

			string[] parts = typeValue.Split('#');

			if (parts.Length != 2) { return; }

			string type = parts[1];
		
			switch (type) {
			case "Procedure":
				this.id = subject.getId();
				this.title = subject.getValueForProperty ("hasProcedureTitle");
				this.description = subject.getValueForProperty ("hasProcedureDescription");
				break;
			case "Author":
				this.author = new Author(subject);
				break;
			case "Step":
				this.firstStep = new Step(subject);
				break;
			default:
				break;
			}
		}
	}

	public string getId() {
		return this.id;
	}

	public string getTitle() {
		return this.title;
	}

	public string getDescription() {
		return this.description;
	}

	public Author getAuthor() {
		return this.author;
	}

	public Step getFirstStep ()  {
		return this.firstStep;
	}
}

