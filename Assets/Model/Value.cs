﻿using System.Collections;
using System;

//Deprecated
[Serializable]
public class Value
{
	public string value;
	public string type;
	public string datatype;

	public Value(string value, string type, string datatype) {
		this.value = value;
		this.type = type;
		this.datatype = datatype;
	}
}

