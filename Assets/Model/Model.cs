﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Model : MonoBehaviour
{

	Procedure procedure;
	Dictionary<string, Step> steps;

	Stack<Step> navigation = new Stack<Step>();

	// Use this for initialization
	void Start (){}
	
	// Update is called once per frame
	void Update (){}

	public void loadProcedure(Procedure procedure) {
		this.procedure = procedure;
		this.steps = null;
		this.navigation.Clear();

		Step firstStep = procedure.getFirstStep();

		if (firstStep != null) {
			this.navigation.Push(firstStep);
		}
	}

	public void loadSteps(Dictionary<string, Step> steps) {
		this.steps = steps;
	}

	public Step nextStep() {

		if (navigation.Count == 0) { return null;} 
			
		Step currentStep = this.navigation.Peek();

		string nextStepLocalId = currentStep.getNextStep();

		if (this.steps.ContainsKey(nextStepLocalId) == false) { return null;}

		Step step = this.steps[nextStepLocalId];
		this.navigation.Push(step); 
		return step;
	}

	public Step popStep() {
		if (navigation.Count <= 1) { return null; }
		return this.navigation.Pop();

	}

	public Step currentStep() {
		return this.navigation.Peek();
	}

	public Author getAuthor() {
		return this.procedure.getAuthor();
	}
}

