﻿using UnityEngine;
using System.Collections;

public class OntologyTerms
{
	public const string PROCEDURE = "ars:Procedure";
	public const string STEP = "ars:Step";
	public const string AUTHOR = "ars:Author";
}

