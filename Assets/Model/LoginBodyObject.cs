﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class LoginBodyObject
{
	public string username;
	public string password;

	public LoginBodyObject(string username,string password) {
		this.username = username;
		this.password = password;
	}


}

