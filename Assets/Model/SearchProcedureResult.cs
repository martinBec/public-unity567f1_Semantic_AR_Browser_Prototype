﻿using System.Collections;

public class SearchProcedureResult
{

	private string localId;
	private string title;

	public SearchProcedureResult(Procedure procedure) {
		this.localId = procedure.getId();
		this.title = procedure.getTitle();
	}

	public string getTitle() {
		return this.title;
	}

	public string getID() {
		return this.localId;
	}
}

