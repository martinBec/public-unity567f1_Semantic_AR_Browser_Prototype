﻿using System.Collections;
using System.Collections.Generic;

//Deprecated
public class Subject
{
	string localId;

	Dictionary<string,Property> properties;

	public Subject(string localId,List<Property> properties) {
		this.localId = localId;
		this.properties = new Dictionary<string,Property> ();
		foreach (Property p in properties) {
			this.properties.Add(p.getId (), p);
		}
	}

	public string getId() {
		return localId;
	} 

	public Property getPropertyFor(string localId) {
		if (properties.ContainsKey(localId)) {
			return properties[localId];
		}
		else {
			return null;
		}
	}

	public string getValueForProperty(string localId) {
		Property property = this.getPropertyFor(localId);
		if (property != null) {
			return property.getValue().value;
		}
		return "";
	}
}

