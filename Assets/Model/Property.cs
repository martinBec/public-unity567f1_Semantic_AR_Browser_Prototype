﻿using System.Collections;
using System;
using System.Collections.Generic;

//Deprecated
public class Property
{

	private string localId;

	private Value[] values;

	public Property(string localId,List<Value> values) {
		this.localId = localId;
		this.values = values.ToArray();
	}

	public string getId() {
		return this.localId;
	}

	public Value getValue() {
		return this.getValueAt (0);
	}

	public Value getValueAt(int index) {
		if (index < this.values.Length) {
			return this.values[index];
		}
		return null;
	}
}

