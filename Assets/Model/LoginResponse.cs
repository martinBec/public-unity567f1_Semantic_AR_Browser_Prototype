﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class LoginResponse
{
	public string username;
	public string token;
}

