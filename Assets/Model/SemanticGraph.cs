﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

public class SemanticGraph
{
	public const string GRAPH = "@graph";
	public const string CONTEXT = "@context";

	public JSONArray statements;
	public JSONNode context;

	private SemanticGraph (JSONArray statements, JSONNode context) {
		this.statements = statements;
		this.context = context;
	}

	public static SemanticGraph parse(string jsonStr)
	{
		JSONNode json = SimpleJSON.JSON.Parse(jsonStr);
		return SemanticGraph.init(json);
	}

	public static SemanticGraph init(JSONNode graphJson) 
	{
		SemanticGraph graph;

		if (graphJson.HasKey(GRAPH)) {
			graph = SemanticGraph.fromGraphJSON(graphJson);
		}
		else if (graphJson.HasKey(CONTEXT)) {
			graph = SemanticGraph.fromJSON(graphJson);
		}
		else {
			graph = new SemanticGraph(statements: new JSONArray(), context: new JSONObject());
		}

		return graph;
	}

	private static SemanticGraph fromGraphJSON(JSONNode graphJson) 
	{
		JSONArray statements = new JSONArray();
		JSONNode context = new JSONObject();

		statements = graphJson[GRAPH].AsArray;
		context = graphJson[CONTEXT].AsObject;
			
		return new SemanticGraph (statements, context);;
	}

	private static SemanticGraph fromJSON(JSONNode graphJson) 
	{
		JSONNode context = graphJson.Remove(CONTEXT);
		return new SemanticGraph(graphJson.AsArray, context);
	}
}